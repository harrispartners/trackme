//
//  Tracker.m
//  Tracker
//
//  Created by apple on 10/30/15.
//
//

#import "Tracker.h"

@implementation Tracker


- (void)track:(CDVInvokedUrlCommand *)command:(CDVInvokedUrlCommand*)command
{
 //   MainClass *appDelegate = (MainClass *)[[UIApplication sharedApplication] delegate];
   // [[[UIApplication sharedApplication] delegate] performSelector:@selector(nameofMethod)];
    
    NSString* callbackId = [command callbackId];
    NSString* name = [[command arguments] objectAtIndex:0];
    NSString* msg = [NSString stringWithFormat: @"Hello, %@", name];
    
    CDVPluginResult* result = [CDVPluginResult
                               resultWithStatus:CDVCommandStatus_OK
                               messageAsString:msg];
    
    [self success:result callbackId:callbackId];
}

@end
