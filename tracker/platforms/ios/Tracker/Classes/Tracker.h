//
//  Tracker.h
//  Tracker
//
//  Created by apple on 10/30/15.
//
//

#import <Cordova/CDV.h>

@interface Tracker : CDVPlugin

- (void) track:(CDVInvokedUrlCommand*)command;

@end
