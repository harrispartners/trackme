/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.harrispartners.phonegap.tracker;

import android.os.Bundle;
import org.apache.cordova.*;
import com.harrispartners.phonegap.tracker.services.TrackerService;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import java.lang.String;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends CordovaActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Set by <content src="index.html" /> in config.xml
        Log.i("zuhaibdev"," Starting app ");
        loadUrl(launchUrl);
        String url = "http://192.168.5.33:8080/track/1/";
        long duration = 1000 *60 *2;

        boolean alarmUp = (PendingIntent.getBroadcast(this, 0,
                new Intent(MainActivity.this, TrackerService.class), PendingIntent.FLAG_NO_CREATE) != null);
        if (!alarmUp) {
            // alarm is set; do some stuff
            Intent intent = new Intent(MainActivity.this, TrackerService.class);
            intent.putExtra("url", url);
            PendingIntent pintent = PendingIntent.getService(this, 0, intent, 0);
            AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
            alarm.setRepeating(AlarmManager.RTC_WAKEUP, new Date().getTime(), duration, pintent);
            Log.i("InSTallation started","Tracker Plugin Installed: ");
        }

    }
}
