package com.harrispartners.phonegap.tracker.tasks;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.harrispartners.phonegap.tracker.rest.Client;

import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by apple on 10/20/15.
 */
public class LocationUpdateTask  extends AsyncTask{
    Activity mActivity;
    String uri;
    JSONObject jsonParam;

    public LocationUpdateTask(JSONObject jsonParam, String scope) {
        //this.mActivity = activity;
        this.uri = scope;
        this.jsonParam = jsonParam;
    }

    protected static final String TAG = "location-updates-sample";
    /**
     * Executes the asynchronous job. This runs when you call execute()
     * on the AsyncTask instance.
     */
    @Override
    protected Void doInBackground(Object[] objects) {
        try {
            Log.i(TAG, "Zuhaib Provider update  " + objects[0]);
            String token = "";
            JSONObject jsonObject = (JSONObject) objects[1];
            String uri = (String) objects[0];
            if (jsonObject != null) {
                // **Insert the good stuff here.**
                // Use the token to access the user's Google data.
                Client restClient = new Client();
                restClient.sendRequest(uri, jsonObject);
            }

        } catch (IOException e) {
            e.printStackTrace();
            Log.i(TAG, e.getMessage());
            // The fetchToken() method handles Google-specific exceptions,
            // so this indicates something went wrong at a higher level.
            // TIP: Check for network connectivity before starting the AsyncTask.

        }
        return null;
    }



}
