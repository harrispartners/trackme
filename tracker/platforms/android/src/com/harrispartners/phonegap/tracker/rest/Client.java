package com.harrispartners.phonegap.tracker.rest;

import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by apple on 10/20/15.
 */
public class Client {



    public void sendRequest(String uri, JSONObject jsonParam)throws IOException{
        Log.i("****", "JSON sending "+jsonParam.toString());
        // Create a new HttpClient and Post Header
        HttpURLConnection urlConnection = null;
           URL url = new URL(uri);
            urlConnection = (HttpURLConnection) url.openConnection();
            try {
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("User-Agent", "T (harrispartners Android ) Tracker");

                // "Url":"http://google.com/","Agent":"?", "Source":"future-you","Lng":lng, "Lat":lat
            /*
            "http://192.168.5.33:8080/track/1/")
            jsonParam.put("Url", "http://google.com/");
            jsonParam.put("Agent", "?");
            jsonParam.put("Source", "future-you");
            jsonParam.put("Lng", loc.getLongitude());
            jsonParam.put("Lat", loc.getLatitude());
            */
                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(jsonParam.toString());
                wr.flush();
                wr.close();
                Log.i("****", "JSON " + jsonParam.toString());

                int status = urlConnection.getResponseCode();

                switch (status) {
                    case 200:
                    case 201:
                        BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                        StringBuilder sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line+"\n");
                        }
                        br.close();
                        Log.i("****", "Response " + sb.toString());
                }



        } catch (Exception ex) {
           // ex.printStackTrace();
        } finally {
            if (null != urlConnection) {
                urlConnection.disconnect();
            }
        }


    }



}
