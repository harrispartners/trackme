package com.harrispartners.phonegap.tracker;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.harrispartners.phonegap.tracker.services.TrackerService;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import java.lang.String;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by apple on 10/22/15.
 */
public class TrackerPlugin extends CordovaPlugin {
    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        if ("track".equals(action)) {
            final long duration = args.getLong(0);
            final String url = args.getString(1);
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    callbackContext.success(); // Thread-safe.
                    boolean alarmUp = (PendingIntent.getBroadcast(cordova.getActivity(), 0,
                            new Intent(cordova.getActivity(), TrackerService.class), PendingIntent.FLAG_NO_CREATE) != null);
                    if (!alarmUp) {
                        // alarm is set; do some stuff
                        Intent intent = new Intent(cordova.getActivity(), TrackerService.class);
                        intent.putExtra("url", url);
                        PendingIntent pintent = PendingIntent.getService(cordova.getActivity(), 0, intent, 0);
                        AlarmManager alarm = (AlarmManager) cordova.getActivity().getSystemService(Context.ALARM_SERVICE);
                        alarm.setRepeating(AlarmManager.RTC_WAKEUP, new Date().getTime(), duration, pintent);
                        callbackContext.success("Tracker Plugin Installed: ");
                    }


                }
            });
            return true;
        }
        return false;
    }

    @Override
    public void onPause(boolean multitasking) {
        super.onPause(multitasking);
    }

    @Override
    public void onResume(boolean multitasking) {
        super.onResume(multitasking);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}