window.onerror = function(message, url, lineNumber) {
    console.log("Error: "+message+" in "+url+" at line "+lineNumber);
}


//Uncomment the line below to store the Facebook token in localStorage instead of sessionStorage
//openFB.init('YOUR_FB_APP_ID', 'http://localhost/openfb/oauthcallback.html', window.localStorage);

var app = {
    // Application Constructor
initialize: function() {
    this.bindEvents();
},
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
    document.addEventListener('offline', this.onDeviceReady, false);
},
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
onDeviceReady: function() {
    console.log("started");
  
   console.log("finito");
    app.receivedEvent('deviceready');
},
onProgress: function(prog) {
    console.log('prog: ' + prog);
},
onFbError: function(error) {
    console.log('code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
},
log: function(text){
    var log = document.getElementById("log");
    log.innerHTML = text;
},
rest: function (url,json){
    // console.log("Starting now ")
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function()
    {
        if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
        {
            console.log(xmlHttp.responseText);
        }
   
    }
    xmlHttp.open("POST", url, true);
    xmlHttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlHttp.send(JSON.stringify(json))
},
onFbSuccess: function(response) {
    console.log(response);
    if(response.status == "connected"){
   /*
    * {
    status: "connected",
    authResponse: {
        session_key: true,
        accessToken: "<long string>",
        expiresIn: 5183979,
        sig: "...",
        secret: "...",
        userID: "634565435"
    }
}
    */
    }
},
    // Update DOM on a Received Event
receivedEvent: function(id) {
    var parentElement = document.getElementById(id);
    var receivedElement = parentElement.querySelector('.received');
    /*
    document.getElementById("fbbtn").addEventListener("click", 
    		facebookConnectPlugin.login(["user_about_me", "user_birthday", "user_education_history", "user_work_history", "user_friends", "email", "user_likes" ], app.onFbSuccess, app.onFbError),     		
    		false);
    */
    // listeningElement.setAttribute('style', 'display:none;');
    
    //listeningElement.innerHTML = 'Zuhaib'
    
   
    
    receivedElement.setAttribute('style', 'display:block;');
    
    console.log('Rec Event is: ' + id);
}
};

app.initialize();