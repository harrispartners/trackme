window.onerror = function(message, url, lineNumber) {
    console.log("Error: "+message+" in "+url+" at line "+lineNumber);
}


//Uncomment the line below to store the Facebook token in localStorage instead of sessionStorage
//openFB.init('YOUR_FB_APP_ID', 'http://localhost/openfb/oauthcallback.html', window.localStorage);

var app = {
    // Application Constructor
initialize: function() {
    this.bindEvents();
},

bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
    document.addEventListener('offline', this.onDeviceReady, false);
},

onDeviceReady: function() {
    console.log("started");
  
   console.log("finito");
    app.receivedEvent('deviceready');
},
onProgress: function(prog) {
    console.log('prog: ' + prog);
},
onFbError: function(error) {
    console.log('code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
},
log: function(text){
    var log = document.getElementById("log");
    log.innerHTML = text;
},
rest: function (url,json,update){
	$.ajax({
		type: 'POST',
		url: url,
		crossDomain: true,
		data: JSON.stringify(json),
		dataType: 'json',
		success: function(responseData, textStatus, jqXHR) {
			console.log(responseData);
			update(responseData);
		},
		error: function (responseData, textStatus, errorThrown) {
			update(responseData);
		}
		});
},
friendsCount: function(c){
	if(c.name == undefined){
		$("#deviceready").html("Error");
	}else{
		var txt = "Hello "+c.name+" Your friends count: "+c.friends.summary.total_count
		$("#deviceready").html(txt);
	}
},
receivedEvent: function(id) {
    var parentElement = document.getElementById(id);
    var receivedElement = parentElement.querySelector('.received');
    receivedElement.setAttribute('style', 'display:block;');
    console.log('Rec Event is: ' + id);
}
};

app.initialize();